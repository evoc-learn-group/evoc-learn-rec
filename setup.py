#!/usr/bin/env python
# -*- coding: utf-8 -*-
from os.path import join as pjoin
from setuptools import setup, find_namespace_packages

with open(pjoin("evoclearn", "rec", "version.py")) as infh:
    version_py = {}
    exec(infh.read(), version_py)
    package_name = version_py["package_name"]
    version = version_py["__version__"]

python_requires = ">=3.7"

install_requires = [
    "click",
    "keras",
    "tensorflow",
    "evoclearn-core>=0.18.5,<0.19.0",
    #For serialisation consistency:
    "dill>=0.3.4,<0.4.0",
    "pandas>=1.3.5,<1.4.0"
]


extras_require = {
    "dev": ["ipython",
            "matplotlib"]
}


setup_args = {
    "name": package_name,
    "version": version,
    "description": "Syllable recogniser for Early Vocal Learning simulation models.",
    "long_description": "Syllable recogniser for Early Vocal Learning simulation models...",
    "author": "The EVocLearn Group",
    "author_email": "daniel.r.vanniekerk@gmail.com",
    "url": "https://gitlab.com/evoc-learn-group/evoc-learn-rec",
    "packages": find_namespace_packages(include=["evoclearn.*"]),
    "python_requires": python_requires,
    "install_requires": install_requires,
    "extras_require": extras_require,
    "package_data": {"": ["*.json", "*.xml"]},
    "entry_points": {
        "console_scripts": [
            "evl_rec=evoclearn.rec.cli:main",
        ]
    }
}

setup(**setup_args)
