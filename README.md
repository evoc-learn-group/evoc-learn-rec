Evoclearn-rec
=============

This is a Python package that provides tools to train and apply syllable encoders/recognisers for use in the [Evoclearn project](https://evoc-learn.gitlab.io/).

The repository also contains some pretrained models (in the [etc](https://gitlab.com/evoc-learn-group/evoc-learn-rec/-/tree/master/etc) sub-directory) based on the TIMIT and Librispeech corpora.

This package can be used for mapping audio to syllable percept vectors directly **OR** as a library for other downstream packages such as [evoc-learn-opt](https://gitlab.com/evoc-learn-group/evoc-learn-opt) which implements goal-directed vocal exploration.

If you use these tools, please cite:

```
@inproceedings{xu22_evoclearn,
  title = {{Evoc-Learn -- High quality simulation of early vocal learning}},
  author = {Xu, Y. and Xu, A. and Van Niekerk, D. R. and Gerazov, B. and Birkholz, P. and Krug, P. B. and Prom-on, S. and Halliday, L. F.},
  booktitle = {{Proc. Interspeech}},
  year = {2022},
  month = sep,
  address = {Incheon, South Korea},
  pages = {3665--3666}
}
```

## Getting started

#### Installation

Install the latest release version from the *Python Package Index* with:

```bash
pip install evoclearn-rec
```

Or build a package from this source repository and install with:

```bash
python setup.py bdist_wheel
pip install dist/evoclearn_rec-0.5.3-py3-none-any.whl
```

You will need Python [setuptools](https://pypi.org/project/setuptools/), [wheel](https://pypi.org/project/wheel/).


#### Using a pre-trained model

Apply a pre-trained model on an audio file from the command line or Python as follows or train your own model as detailed below.

- Applying a model from the command line (example):
  ```bash
  mkdir out/
  evl_rec ac2vec etc/model_2.ac2vec.json.bz2 out/ ref_data/wavs/*.wav
  ```

- Loading and applying a model from Python:
  ```python
  from evoclearn.rec.ac2vec import Ac2Vec
  from evoclearn.core.io import load_audio
  wav = load_audio("ref_data/wavs/b-ah1_5304-109507-0046_21537.wav")
  model = Ac2Vec.from_file("etc/model_2.ac2vec.json.bz2")
  vec = next(model([wav], from_wav=True))
  ```

## Training your own syllable encoder

Here follows an example of how to use the command line tools to train a CV encoder on TIMIT data.

#### Prepare data

Firstly extract the train and validation samples to WAV files in two directories, with the phone symbols represented at the start of the filename in the following format:

```bash
${CONSONANT}-${VOWEL}_${REST_OF_FILENAME}.wav
```

Here is an example of the partial directory contents of the TIMIT validation set:

```
b-aa_00_dr1_msjs1_sx369.wav   b-ih_00_dr4_fgjd0_sx279.wav   d-aa_00_dr2_mabw0_sa1.wav     d-ah_00_dr2_mwvw0_si1476.wav  d-iy_00_dr8_fmld0_si925.wav
b-aa_00_dr2_fpas0_sx404.wav   b-ih_00_dr4_fjmg0_sx371.wav   d-aa_00_dr2_mbjk0_sa1.wav     d-ah_00_dr3_mhpg0_si460.wav   d-iy_00_dr8_mdaw1_si1453.wav
b-aa_00_dr2_mabw0_sx404.wav   b-ih_00_dr4_flbw0_sx319.wav   d-aa_00_dr2_mccs0_sa1.wav     d-ah_00_dr3_mtdt0_sa1.wav     d-iy_00_dr8_mjln0_si819.wav
b-aa_00_dr2_mgwt0_sx369.wav   b-ih_00_dr4_fnmr0_sx319.wav   d-aa_00_dr2_mcem0_sa1.wav     d-ah_00_dr4_mdrm0_si1643.wav  d-iy_00_dr8_mjln0_sx99.wav
```

**Notes:**

 - I simply used the alignments provided in TIMIT, extracting the audio from the **start of the closure** to the end of the vowel.
 - If you'd like to use the default feature settings (step size and maximum length), **ensure that all wav files have duration < 0.6s**
 - Bear in mind that the samplerate in TIMIT is 16kHz (the default settings in the tools consider this)


#### Create a working directory for training

When the data is prepared, the first step is to standardise the data into a working directory for convenient use by the training script. This step currently does the following:

 1. **Feature extraction** using evoclearn-core and the supplied feature settings
 2. Standardises the **input length** of the audio by padding
 3. Extracts **feature statistics** to define the feature normalisation function (z-score)
 4. Creates a standardised **working directory** structure to be used by the training script.

Given the prepared train and validation data in two sub-directories, do the following:

```bash
mkdir working
evl_rec collect etc/feat_settings.json ${TRAIN_WAVS_DIR} working/train_
evl_rec collect etc/feat_settings.json ${VALID_WAVS_DIR} working/valid_
```

The contents of the working directory should then look like this:

```bash
train_paths.txt  train_x.tracks.h5  train_x_stats.json  train_y.npz
valid_paths.txt  valid_x.tracks.h5  valid_x_stats.json  valid_y.npz
```

#### Train the recogniser

Run the training script on the working directory (actually a path prefix: the forward-slash is necessary):

```bash
evl_rec train working/
```

The working directory should look something like this after training:

```bash
model.202009221619.v0-1-0_b32_l121_e20.h5    train.npz        train_x.tracks.h5   train_y.npz  valid_paths.txt    valid_x_stats.json  vec_to_syms.func.dl
model.202009221619.v0-1-0_b32_l121_e20.json  train_paths.txt  train_x_stats.json  valid.npz    valid_x.tracks.h5  valid_y.npz         x_norm.func.dl

```

**Notes:**

 - The number of epochs and batch sizes can be changed, to see the script options do `evl_rec train --help`
 - The training script also dumps some functions (saved as `*.dl`) that are necessary when the model is applied.
 - Two *cache files* (`train.npz` and `valid.npz`) containing normalised training data is dumped and can be reused in subsequent runs (see script options)


#### Compile the model and support settings/functions for application

To apply the model to input wave files we would need to know how to do feature extraction, apply normalisation and resize the input data according to the batch size. For convenience the compile script combines all the resources needed in a single file, for example:

```bash
evl_rec compile \
    etc/feat_settings.json \
    working/x_norm.func.dl \
    working/vec_to_syms.func.dl \
    working/model.202009221619.v0-1-0_b32_l121_e20.h5 \
    model.ac2vec.json.bz2
```

**Notes:**

 - The `--max_feat_len` and `--batchsize` parameters have defaults but also need to be selected to match the training conditions.


#### Apply the model to audio files

Load the model compiled in the previous step and apply it directly to audio files with the ac2vec script (output can be the floating point vector or highest scoring symbols):

```bash
#output vector:
$ evl_rec ac2vec model.ac2vec.json.bz2 vec_ wavs_valid/b-aa_00_dr1_msjs1_sx369.wav
$ cat vec_wavs_valid_b-aa_00_dr1_msjs1_sx369.wav.txt
0.9814603    0.007172346    0.008451998    0.92077905    0.0053934157    0.0050417185    0.033332318    2.5252919e-05    5.8752853e-06    8.8958495e-06    8.6941254e-05

#output symbols:
$ evl_rec ac2vec --outputsyms ac2vec_model.json.bz2 sym_ wavs_valid/b-aa_00_dr1_msjs1_sx369.wav
$ cat sym_wavs_valid_b-aa_00_dr1_msjs1_sx369.wav.txt
b-aa
```

**Notes:**

 - Audio files must result in features length shorter than `--max_feat_len` (0.6s with the default setup)
 - Will currently downsample inputs to 16kHz because this matches the TIMIT model conditions
