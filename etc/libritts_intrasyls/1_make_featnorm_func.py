#!/usr/bin/env python

from functools import partial
import json

import dill
import pandas as pd
import numpy as np

from evoclearn.core import utils, mappings, Track


if __name__ == "__main__":
    max_feat_len = json.load(open("compile_settings.json"))["max_feat_len"]
    feat_stats = pd.read_json("feat_stats.json")
    feat_stats.columns = list(map(str, feat_stats.columns))
    padding = np.zeros((1, feat_stats.shape[1]), dtype=np.float32)

    normfunc = utils.compose_funcs(partial(mappings.normalise_standard,
                                           stats=feat_stats),
                                   partial(Track.fix_length,
                                           n=max_feat_len,
                                           padding=padding,
                                           location="back"))
    with open("featnorm.func.dl", "wb") as outfh:
        dill.dump(normfunc, outfh)
