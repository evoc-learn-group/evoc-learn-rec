#!/usr/bin/env python

from functools import partial
import json

import dill

from evoclearn.rec import vec2sym

if __name__ == "__main__":
    onsets = json.load(open("onsets.json"))
    vowels = json.load(open("vowels.json"))
    offsets = json.load(open("offsets.json"))
    onsets_inv = {v:k for k, v in onsets.items()}
    vowels_inv = {v:k for k, v in vowels.items()}
    offsets_inv = {v:k for k, v in offsets.items()}
    onsetlabs = [onsets_inv[i] for i in range(len(onsets_inv))]
    vowellabs = [vowels_inv[i] for i in range(len(vowels_inv))]
    offsetlabs = [offsets_inv[i] for i in range(len(offsets_inv))]
    segdims = (len(onsetlabs), len(vowellabs), len(offsetlabs))
    veclabels = onsetlabs + vowellabs + offsetlabs
    v2s = partial(vec2sym.vec_to_syms, segdims=segdims, veclabels=veclabels)
    s2v = partial(vec2sym.syms_to_vec, segdims=segdims, veclabels=veclabels)

    with open("vec2sym.func.dl", "wb") as outfh:
        dill.dump(v2s, outfh)

    with open("sym2vec.func.dl", "wb") as outfh:
        dill.dump(s2v, outfh)
