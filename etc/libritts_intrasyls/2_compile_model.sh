#!/bin/bash

evl_rec compile \
    --max_feat_len `jq -r ".max_feat_len" compile_settings.json` \
    --batchsize `jq -r ".batchsize" compile_settings.json` \
    --unittype `jq -r ".unittype" compile_settings.json` \
    --segdims `jq -r ".segdims" compile_settings.json` \
    --outlabs `jq -r ".outlabs" compile_settings.json` \
    --samplerate `jq -r ".samplerate" compile_settings.json` \
    `jq -r ".featsettingsfile" compile_settings.json` \
    `jq -r ".featnormfuncfile" compile_settings.json` \
    `jq -r ".vec2symfuncfile" compile_settings.json` \
    `jq -r ".modelfile" compile_settings.json` \
    model_2.ac2vec.json.bz2
