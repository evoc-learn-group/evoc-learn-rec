#syntax=docker/dockerfile-upstream:master-experimental
FROM tensorflow/tensorflow:2.2.0-gpu

LABEL Maintainer="Daniel van Niekerk <daniel.r.vanniekerk@gmail.com>"
LABEL Description="Environment for training Keras models using Tensorflow"

VOLUME /mnt/ext

ENV USERNAME=demitasse
ENV USERHOME=/home/$USERNAME
ENV EVOCLEARN_LOG_DIR=/mnt/ext
ENV EVOCLEARN_LOG_LEVEL=10

RUN useradd -ms /bin/bash --uid 1000 $USERNAME

RUN  apt-get -yq update && \
        apt-get -yqq install ssh git libsndfile1 emacs-nox && \
        chown -R $USERNAME:$USERNAME $USERHOME

USER $USERNAME
WORKDIR $USERHOME

RUN --mount=type=ssh,uid=1000 mkdir -p -m 0700 $USERHOME/.ssh && \
        ssh-keyscan gitlab.com >> $USERHOME/.ssh/known_hosts && \
        git clone git@gitlab.com:evoc-learn-group/evoc-learn-core.git && \
        git clone git@gitlab.com:evoc-learn-group/evoc-learn-asr.git

RUN cd $USERHOME/evoc-learn-core && \
        python setup.py bdist_wheel && \
        cd $USERHOME/evoc-learn-asr/evl_rec && \
        python setup.py bdist_wheel && \
        pip install --find-links $USERHOME/evoc-learn-core/dist dist/*.whl && \
        echo "export PATH=$USERHOME/.local/bin:$PATH" >> $USERHOME/.bashrc

WORKDIR /mnt/ext
